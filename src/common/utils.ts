export async function asyncForeach(array: any[], callback: Function) {
    try {
        for (let i = 0; i < array.length; i++) {
            await callback(array[i], i);
        }
    } catch (e) {
        throw e;
    }
}
