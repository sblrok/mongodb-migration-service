import {promisify} from 'util';
import {asyncForeach} from './common/utils';
import * as readline from 'readline'
import IMigration from './models/IMigration';
import IDatabaseMigration from './models/IDatabaseMigration';
import mongoose from 'mongoose';
const fs = require('fs');

export default class MongoMigrationService {
    private readDir = promisify(fs.readdir);
    private statusText = 'Migration completed ';
    private get migrationsCollection() {
        return mongoose.connection.db.collection('migrations');
    }
    public readonly migrationsPath: string;

    constructor(migrationsPath: string) {
        this.migrationsPath = migrationsPath;
    }

    public async synchronize() {
        if (!fs.existsSync(this.migrationsPath)) {
            throw new Error('Migration path doesn`t exist!');
        }
        const lastMigration = await this.getLastMigration();

        const files = await this.readDir(this.migrationsPath);
        const migrations = files
            .filter((file: string) => {
                const extensionParts = file.split('.');
                const extension = extensionParts[extensionParts.length - 1];
                const timestamp = file.split(/[.\-]/g)[0];
                return extension === 'js' && !isNaN(+timestamp) && (!lastMigration || lastMigration &&
                     +timestamp > lastMigration.timestamp);
            })
            .sort((a: string, b: string) =>
                +a.split(/[.\-]/g)[0] - +b.split(/[.\-]/g)[0]);

        await asyncForeach(migrations, async (file: string, index: number) => {
            const migration: IMigration = require(this.migrationsPath + '/' + file);
            await migration.run(mongoose.connection.db);
            await this.migrationsCollection.insertMany([<IDatabaseMigration>{
                name: file,
                timestamp: +file.split(/[.\-]/g)[0]
            }]);

            readline.clearLine(process.stdout, 0);
            readline.cursorTo(process.stdout, 0);
            process.stdout.write(this.statusText + (index + 1) + '/' + migrations.length + '\n');
        });
    }

    public async rollback() {
        if (!fs.existsSync(this.migrationsPath)) {
            throw new Error('Migration path doesn`t exist!');
        }

        const lastMigrationObject = await this.getLastMigration();
        if (lastMigrationObject) {
            const migration: IMigration = require(this.migrationsPath + '/' + lastMigrationObject.name);
            await migration.rollback(mongoose.connection.db);

            await this.migrationsCollection.deleteOne({timestamp: lastMigrationObject.timestamp});
        }
    }

    private async getLastMigration(): Promise<IDatabaseMigration | null> {
        return await this.migrationsCollection.findOne({},
            {sort: {timestamp: -1}});
    }
}
