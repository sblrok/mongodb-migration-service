import {Db} from 'mongodb';

export default interface IMigration {
    run(db: Db): void;
    rollback(db: Db): void;
}
