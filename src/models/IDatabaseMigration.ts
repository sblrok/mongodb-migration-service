export default interface IDatabaseMigration {
    name: string;
    timestamp: number;
}
