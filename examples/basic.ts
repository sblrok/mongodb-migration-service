import MongoMigrationService from '../src/main';
import mongoose from 'mongoose';

export default class Basic {
    public async start() {
        const testCollection = mongoose.connection.db.collection('testCollection');
        const migrationService = new MongoMigrationService(__dirname + '/migrations');

        // synchronizing existing migrations
        await migrationService.synchronize();

        const firstResult = await testCollection.find({}).toArray();
        console.log('First: ', firstResult);

        // rollback last migration
        await migrationService.rollback();

        const secondResult = await testCollection.find({}).toArray();
        console.log('Second: ', secondResult);

        await migrationService.rollback();

        const thirdResult = await testCollection.find({}).toArray();
        console.log('Third: ', thirdResult);
    }
}
