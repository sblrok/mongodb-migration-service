import {describe} from 'mocha';
import Basic from './basic';
import mongoose from 'mongoose';

describe('Testing the main functionality', () => {
    before(async () => {
        const uri = 'mongodb://localhost:27017/test';
        const options = { useNewUrlParser: true };
        await mongoose.connect(uri, options);
    });

    it('Basic', async () => {
        const basicTest = new Basic();
        await basicTest.start();
    });

    after(async () => {
        await mongoose.connection.db.dropDatabase();
    });
});
