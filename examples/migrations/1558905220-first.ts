import {Db} from 'mongodb';

export async function run(db: Db) {
    await db.collection('testCollection').insertMany([{
        Name: 'Hello',
        Value: 'World'
    }]);
}

export async function rollback(db: Db) {
    await db.collection('testCollection').deleteOne({Name: 'Hello', Value: 'World'});
}
