import {Db} from 'mongodb';

export async function run(db: Db) {
    const testValue = await db.collection('testCollection').findOne({Name: 'Hello', Value: 'World'});
    testValue.Value = 'Tom';
    await db.collection('testCollection').updateOne({Name: 'Hello', Value: 'World'}, {$set: testValue});
}

export async function rollback(db: Db) {
    const testValue = await db.collection('testCollection').findOne({Name: 'Hello', Value: 'Tom'});
    testValue.Value = 'World';
    await db.collection('testCollection').updateOne({Name: 'Hello', Value: 'Tom'}, {$set: testValue});
}
