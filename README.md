# mongodb-migration-service

Service for automatic migration for Node applications with MongoDB

## Getting Started

Service works with migrations, they should have two functions, **run** and **rollback**  

For example:  

```typescript
import {ConnectionBase} from 'mongoose';

export async function run(db: ConnectionBase) {
    await db.collection('testCollection').insertMany([{
        Name: 'Hello',
        Value: 'World'
    }]);
}

export async function rollback(db: ConnectionBase) {
    await db.collection('testCollection')
        .deleteOne({Name: 'Hello', Value: 'World'});
}
```

Before starting synchronization, you must have an existing connection to MongoDB.  
It is necessary to set the folder with migrations to the service.

```typescript
import MongoMigrationService from 'mongo-migration-service';

class App {
    private migrationService: MongoMigrationService;
    constructor() {
        this.migrationService = new MongoMigrationService(__dirname + '/migrations');
    }
}
```


To run migrations, you must call the function **synchronize**  
Migrations are running in the order they are created.  
Each migration file has the format {timestamp}[-{name}].ts  

```typescript
await migrationService.synchronize();
```

After the migration is complete, information is written to the 'migrations' collection in the database.  
Each time the **synchronize** method starts, it checks existing migrations in the database.  

To undo the last migration, use the **rollback** method.

```typescript
await migrationService.rollback();
```

Example can be found in the [examples](https://gitlab.com/sblrok/mongodb-migration-service/tree/master/examples) project folder.
